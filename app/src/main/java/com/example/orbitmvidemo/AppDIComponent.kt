package com.example.orbitmvidemo

import android.app.Application
import com.example.orbitmvidemo.data.DataDIModule
import com.example.orbitmvidemo.domain.DomainDIModule
import com.example.orbitmvidemo.presentation.di.PresentationDIModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [
        DataDIModule::class,
        DomainDIModule::class,
        PresentationDIModule::class,
        AppDIModule::class
    ]
)

@Singleton
interface AppDIComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppDIComponent
    }

    fun inject(app: App)
}