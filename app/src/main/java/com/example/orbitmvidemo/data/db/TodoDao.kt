package com.example.orbitmvidemo.data.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface TodoDao {
    @Query("SELECT * FROM todo")
    fun getAll(): Single<List<TodoEntity>>

    @Insert
    fun insert(entity: TodoEntity): Completable

    @Delete
    fun delete(entity: TodoEntity): Completable
}