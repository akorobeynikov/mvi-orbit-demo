package com.example.orbitmvidemo.data.todo

import com.example.orbitmvidemo.data.db.TodoEntity
import com.example.orbitmvidemo.domain.todo.model.Todo

internal object TodoMapper {
    fun mapTodo(todo: Todo): TodoEntity {
        return TodoEntity(todo.id, todo.text)
    }

    fun mapTodoEntity(todo: TodoEntity): Todo {
        return Todo(todo.uid, todo.text)
    }
}