package com.example.orbitmvidemo.presentation.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.orbitmvidemo.databinding.ActivityMainBinding
import com.example.orbitmvidemo.presentation.main.action.AddAction
import com.example.orbitmvidemo.presentation.main.action.RemoveAction
import com.example.orbitmvidemo.presentation.main.rv.TodoRvAdapter
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val rvAdapter: TodoRvAdapter = TodoRvAdapter()

    @Inject
    lateinit var viewModel: TodoViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRv()
        rvAdapter.onRemoveClickListener = {
            viewModel.sendAction(RemoveAction(it))
        }

        binding.addButton.setOnClickListener {
            val text = binding.textField.text.toString()
            viewModel.sendAction(AddAction(text))
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.connect(this, ::handleState)
    }

    private fun handleState(state: TodoState) {
        rvAdapter.data = state.todos
    }

    private fun setupRv() {
        binding.todoRv.apply {
            adapter = rvAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
    }
}
