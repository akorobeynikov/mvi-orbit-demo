package com.example.orbitmvidemo.presentation.main.rv

import androidx.recyclerview.widget.RecyclerView
import com.example.orbitmvidemo.databinding.ItemTodoBinding

class TodoViewHolder(val binding: ItemTodoBinding) : RecyclerView.ViewHolder(binding.root)