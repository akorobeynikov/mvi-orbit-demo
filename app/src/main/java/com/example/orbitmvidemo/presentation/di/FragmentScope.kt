package com.example.orbitmvidemo.presentation.di

import javax.inject.Scope

@Scope
@MustBeDocumented
annotation class FragmentScope