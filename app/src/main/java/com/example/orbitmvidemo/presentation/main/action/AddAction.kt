package com.example.orbitmvidemo.presentation.main.action

data class AddAction(val text: String)