package com.example.orbitmvidemo.presentation.main

import com.example.orbitmvidemo.domain.todo.model.Todo

data class TodoState(val todos: List<Todo>)