package com.example.orbitmvidemo.presentation.main

import com.babylon.orbit.LifecycleAction
import com.babylon.orbit.OrbitViewModel
import com.example.orbitmvidemo.domain.todo.TodoInteractor
import com.example.orbitmvidemo.presentation.main.action.AddAction
import com.example.orbitmvidemo.presentation.main.action.LoadDataAction
import com.example.orbitmvidemo.presentation.main.action.RemoveAction
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TodoViewModel @Inject constructor(
    private val todoInteractor: TodoInteractor
) : OrbitViewModel<TodoState, Nothing>(TodoState(emptyList()), {

    perform("load data")
        .on(LifecycleAction.Created::class.java, LoadDataAction::class.java)
        .transform {
            eventObservable.compose {
                it.flatMap {
                    todoInteractor.getTodos().toObservable()
                }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
            }
        }
        .reduce { currentState.copy(todos = event) }

    perform("add todo")
        .on(AddAction::class.java)
        .transform {
            eventObservable.compose {
                it.flatMap { addAction ->
                    val text = (addAction as AddAction).text
                    todoInteractor.addTodo(text)
                        .toSingleDefault(0)
                        .toObservable()
                }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
            }
        }
        .loopBack { LoadDataAction }

    perform("remove todo")
        .on(RemoveAction::class.java)
        .transform {
            eventObservable.compose {
                it.flatMap { addAction ->
                    val todo = (addAction as RemoveAction).todo
                    todoInteractor.removeTodo(todo)
                        .toSingleDefault(0)
                        .toObservable()
                }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
            }
        }
        .loopBack { LoadDataAction }
})