package com.example.orbitmvidemo.presentation.main.action

import com.example.orbitmvidemo.domain.todo.model.Todo

data class RemoveAction(val todo: Todo)