package com.example.orbitmvidemo.presentation.di

import dagger.Module
import dagger.android.AndroidInjectionModule

@Module(
    includes = [
        AndroidInjectionModule::class,
        ActivityDIModule::class,
        FragmentDIModule::class,
        PresentationDIModule.Declarations::class
    ]
)
abstract class PresentationDIModule {
    @Module
    internal interface Declarations {
        //TBD
    }
}