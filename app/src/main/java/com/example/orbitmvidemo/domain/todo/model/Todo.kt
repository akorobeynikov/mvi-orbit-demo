package com.example.orbitmvidemo.domain.todo.model

data class Todo(val id: Int, val text: String)