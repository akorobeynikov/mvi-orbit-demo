package com.example.orbitmvidemo.domain

import com.example.orbitmvidemo.domain.todo.TodoInteractor
import com.example.orbitmvidemo.domain.todo.TodoInteractorImpl
import dagger.Binds
import dagger.Module

@Module(includes = [DomainDIModule.Declarations::class])
object DomainDIModule {

    @Module
    internal interface Declarations {
        @Binds
        fun bindTodoInteractor(impl: TodoInteractorImpl): TodoInteractor
    }
}