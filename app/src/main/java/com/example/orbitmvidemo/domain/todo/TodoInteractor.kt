package com.example.orbitmvidemo.domain.todo

import com.example.orbitmvidemo.domain.todo.model.Todo
import io.reactivex.Completable
import io.reactivex.Single

interface TodoInteractor {
    fun getTodos(): Single<List<Todo>>
    fun addTodo(text: String): Completable
    fun removeTodo(todo: Todo): Completable
}
