package com.example.orbitmvidemo.domain.todo

import com.example.orbitmvidemo.domain.todo.model.Todo
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class TodoInteractorImpl @Inject constructor(
    private val todoSource: TodoSource
) : TodoInteractor {
    override fun getTodos(): Single<List<Todo>> {
        return todoSource.getTodos()
    }

    override fun addTodo(text: String): Completable {
        return todoSource.addTodo(Todo(0, text))
    }

    override fun removeTodo(todo: Todo): Completable {
        return todoSource.removeTodo(todo)
    }
}